/**
 * @file
 * Contains the definition of the behaviour jsTagCloudsViews.
 */

(function ($, Drupal, drupalSettings) {
    "use strict";
    /**
     * Attaches the JS for TagClouds views.
     */
    Drupal.behaviors.jsTagCloudsViews = {
        attach: function (context, settings) {
            // Get views params.
            var selector = drupalSettings.tagclouds_views.tagcloud.selector,
                library_name = drupalSettings.tagclouds_views.tagcloud.library_name,
                options = drupalSettings.tagclouds_views.tagcloud.options;
            // Run libraries library.
            $(selector)[library_name](options);
        }
    };
})(jQuery, Drupal, drupalSettings);
