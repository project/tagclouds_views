<?php

/**
 * @file
 * Preprocessors and helper functions.
 */

use Drupal\Component\Utility\Html;

#use Drupal\Component\Utility\String;

/**
 * Prepares variables for view templates.
 *
 * Default template: tagcloud_views_view_tagcloud.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 *   - options: An array of options. Each option contains:
 *     - separator: A string to be placed between inline fields to keep them
 *       visually distinct.
 */
function template_preprocess_tagclouds_views_view_tagcloud(&$variables) {
  $view = $variables['view'];
  $rows = &$variables['rows'];
  $style = $view->style_plugin;
  $options = $style->options;

  // Default settings.
  $subquery = '';
  $variables['item_inline_styles'] = '';
  $variables['link_inline_styles'] = '';
  $variables['wrapper_html_item'] = 'ul';

  // Set libraries and styles settings specific for each library.
  switch ($options['type']) {
    case 'tx3':
      // TX3 library.
      $library_name = 'tx3TagCloud';
      $id = 'jtagcloud';
      $variables['class'] = 'jtagcloud';
      $library_options = ['multiplier' => 2];
      $library_path = 'tagclouds_views/tagclouds_views.tx3_tag_cloud';
      $variables['weight_param'] = 'data-weight';
      $variables['wrapper_inline_styles'] = 'width: 100%;height: 200px;';

      $variables['row_html_item'] = 'li';
      break;

    case 'awesomeCloud':
      // Awesome Cloud library.
      // Set Id of element.
      $id = 'awesomeCloud';
      // Set library name and path.
      $library_name = 'awesomeCloud';
      $library_path = 'tagclouds_views/tagclouds_views.awesomeCloud';

      // Set variable params.
      $variables['class'] = 'awesomeCloud';

      // Html items.
      $variables['wrapper_html_item'] = 'div';
      $variables['row_html_item'] = 'span';

      // Inline styles.
      $variables['wrapper_inline_styles'] = 'border: 1px solid #036; height: 500px; margin: 0px auto;padding: 0; page-break-after: always; page-break-inside: avoid; width: 100%;';

      $variables['weight_param'] = 'data-weight';

      $library_options = [
        "size" => [
          "grid" => 16,
          "normalize" => FALSE,
        ],
        "options" => [
          "color" => "random-dark",
          "rotationRatio" => 0.35,
          "printMultiplier" => 3,
          "sort" => "random",
        ],
        "font" => "'Times New Roman', Times, serif",
        "shape" => "square",
      ];
      break;

    case 'jquery_tagcloud':
      $id = 'tagcloud';
      // Set library name and path.
      $library_name = 'tagcloud';
      $library_path = 'tagclouds_views/tagclouds_views.jquery_tagcloud';

      $library_options = [
        'size' => ['start' => 14, 'end' => 18, 'unit' => 'pt'],
        'color' => ['start' => '#cde', 'end' => '#f52'],
      ];

      // Set variable params.
      $variables['class'] = 'jquery_tagcloud';
      // Html items.
      $variables['wrapper_html_item'] = 'div';
      $variables['row_html_item'] = 'span';

      // Inline styles.
      $variables['wrapper_inline_styles'] = '';
      $variables['link_inline_styles'] = 'color:inherit';

      $subquery = $variables['row_html_item'];
      $variables['weight_param'] = 'rel';
      break;

    default:
      // @todo improve this part.
      return FALSE;
  }

  // Set unique id.
  $variables['id'] = Html::getUniqueId($id);

  // Prepare selector.
  $selector = '#' . $variables['id'];
  if ($subquery) {
    $selector .= ' ' . $subquery;
  }

  // Iterate through the views results.
  foreach ($rows as $id => $row) {
    // @todo Need to be implemented usage of statistics module or the same.
    // Set random data weight values.
    $row->data_weight = rand($id, 100);

    // Get entity title and url.
    $row->title = '';
    if (method_exists($row->_entity, 'getName')) {
      $row->title = $row->_entity->getName();
    }
    if (method_exists($row->_entity, 'getTitle')) {
      $row->title = $row->_entity->getTitle();
    }
    $row->link = $row->_entity->url();
  }

  // Added attached.
  $variables['#attached']['library'][] = $library_path;
  $variables['#attached']['drupalSettings']['tagclouds_views']['tagcloud']['id'] = $variables['id'];
  $variables['#attached']['drupalSettings']['tagclouds_views']['tagcloud']['selector'] = $selector;
  $variables['#attached']['drupalSettings']['tagclouds_views']['tagcloud']['library_name'] = $library_name;
  $variables['#attached']['drupalSettings']['tagclouds_views']['tagcloud']['options'] = $library_options;

  // Add global module libraries.
  $variables['#attached']['library'][] = 'tagclouds_views/tagclouds_views.main';
}
