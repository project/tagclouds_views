<?php
/**
 * @file
 *
 */

namespace Drupal\tagclouds_views\Plugin\views\style;

use Drupal\Core\Annotation\Plugin;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\style\StylePluginBase;

/**
 * TagClouds style plugin.
 *
 * @ingroup views_style_plugins
 *
 * @ViewsStyle(
 *   id = "tagclouds_views",
 *   title = @Translation("TagClouds style"),
 *   help = @Translation("Displays tagclouds views for taxonomy terms."),
 *   theme = "tagclouds_views_view_tagcloud",
 *   theme_file = "tagclouds_views.theme.inc",
 *   display_types = {"normal"}
 * )
 */
class TagClouds extends StylePluginBase {

  /**
   * Overrides Drupal\views\Plugin\Plugin::$usesOptions.
   */
  protected $usesOptions = TRUE;

  /**
   * Does the style plugin allows to use style plugins.
   *
   * @var bool
   */
  protected $usesRowPlugin = FALSE;

  /**
   * Does the style plugin support custom css class for the rows.
   *
   * @var bool
   */
  protected $usesRowClass = TRUE;

  /**
   * Does the style plugin support grouping of rows.
   *
   * @var bool
   */
  protected $usesGrouping = FALSE;

  /**
   * Set default options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    // Set default views style options.
    $options['type'] = ['default' => 'awesomeCloud'];

    return $options;
  }

  /**
   * Render the given style.
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $options = [
      'tx3' => $this->t('TX3 library'),
      'awesomeCloud' => $this->t('awesomeCloud'),
      'jquery_tagcloud' => $this->t('jQuery TagCloud'),
    ];

    $library_exists = tagclouds_views_library_exists('tagclouds_views.tx3_tag_cloud');
    if (!$library_exists) {
      $options['tx3'] .= ' [' . $this->t('Not Installed') . ']';
    }
    $library_exists = tagclouds_views_library_exists('tagclouds_views.awesomeCloud');
    if (!$library_exists) {
      $options['awesomeCloud'] .= ' [' . $this->t('Not Installed') . ']';
    }
    $library_exists = tagclouds_views_library_exists('tagclouds_views.jquery_tagcloud');
    if (!$library_exists) {
      $options['jquery_tagcloud'] .= ' [' . $this->t('Not Installed') . ']';
    }

    $form['type'] = [
      '#type' => 'radios',
      '#title' => $this->t('TagCloudLibraries'),
      '#options' => $options,
      '#default_value' => $this->options['type'],
    ];
  }
}
